const btn = document.querySelector("#hamburger");
const menu = document.querySelector(".nav__menu");

btn.addEventListener("click", () => {
  menu.classList.toggle("open");
});
